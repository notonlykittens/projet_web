<?php 
session_start(); 
require 'utilisateur_class.php';
require 'album_class.php';

//conexion à la bdd
try{
	$bdd = new PDO('mysql:host=localhost;dbname=projet_web;charset=utf8', 'root', '');
	$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
}
catch(Exception $e) {
	echo $e->getMessage();
	return;	
}

$utilisateur = new Utilisateur($bdd);
$utilisateur->charge($_SESSION['pseudo']);
?>

<html>
	<head>
		<title>ajout_album</title>
	</head>
	<body>
		<form method="post" action="" enctype="multipart/form-data">
			titre:<input type="text" name="titre" /><br />
			theme1:<input type="text" name="theme1" /><br />
			theme2:<input type="text" name="theme2" /><br />
			theme3:<input type="text" name="theme3" /><br />
			dateCrea:<input type="date" name="dateCrea" /><br />                <!-- à gérer auto avec date du jour -->
			lieu:<input type="text" name="lieu" /><br />
			(confidentialie:)<input type="text" name="confidentialite" /><br />   <!-- à mettre en truc à cocher-->
			choisir une photo de couverture:<input type="file" name="photoFile" /><br />  <!-- éventuellement pour une photo couverture-->
			<input type="submit" value="envoyer" name="submitbutton" />
		</form>	
		<a href="mesAlbums.php" ><button type=button >retour aux albums</button></a>
	</body>
</html>

<?php
if (isset($_POST['submitbutton'])){
	if (!empty($utilisateur->pseudo)
			AND !empty($_POST['titre'])
			AND !empty($_POST['theme1'])
			AND !empty($_POST['theme2'])
			AND !empty($_POST['theme3'])
			AND !empty($_POST['dateCrea'])
			AND !empty($_POST['lieu'])
			AND !empty($_POST['confidentialite'])
			AND $_FILES['photoFile']['error'] == 0){
				
		

		//on vérifie qu'on a bien une image
		$extensions_valides = array( 'jpg' , 'jpeg' , 'gif' , 'png' );
		$extension_upload = strtolower(  substr(  strrchr($_FILES['photoFile']['name'], '.')  ,1)  );
		if ( in_array($extension_upload,$extensions_valides) ){
			//on déplace le fichier uploadé dans le dossier photo du serveur
			$id = md5(uniqid(rand(), true));
			$chemin = "photos/{$id}.{$extension_upload}";
			$resultat = move_uploaded_file($_FILES['photoFile']['tmp_name'],$chemin);
		}

		//on vérifie que le nom de l'album est unique
		$reqalbum = $bdd->prepare("SELECT * FROM album WHERE titre = ?");
		$reqalbum->execute(array($_POST["titre"]));
		if($reqalbum->rowCount() == 0){
			
			//on créé l'id de l'album avec le créateur + le titre
			$id = $utilisateur->pseudo;
			$id .=$_POST["titre"];
			
			//création d'un objet monalbum avec les spécificités voulues du form
			$infos = array ('chemPhotoCouv' => $chemin,
							'createur' => $utilisateur->pseudo,
							'theme1' => $_POST["theme1"],
							'theme2' => $_POST["theme2"],
							'theme3' => $_POST["theme3"],
							'titre' => $_POST["titre"],
							'dateCrea' => $_POST["dateCrea"],
							'IDalbum' => $id,
							'nbphotos' => 1,						//à gérer avec une méthode qui retourne le nbphoto actuel
							'confidentialite' => $_POST["confidentialite"]);
		echo $infos['theme1'];
			$monalbum = new Album($bdd);
			$monalbum->remplie($infos);
		echo $monalbum->theme1;
			$monalbum->ajoutAbdd();
			echo "\nphoto uploadee !";	
		}
		else{
			echo "album déjà existant";
		}
	}
	else{
		echo "veuillez remplir tous les champs";
	}
}
else{
	//echo "pas isset submitbutton";
}

?>