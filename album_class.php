<?php
class Album {
	// attibuts
	public $chemPhotoCouv;
	public $createur;
	public $theme1;
	public $theme2;
	public $theme3;
	public $titre;
	public $dateCrea;
	public $IDalbum;
	public $confidentialite;
	public $nbphotos;
	
	
	private $bdd;
	
	//geters
	public function getChemPhotoCouv() {return $this->chemPhotoCouv;}
	public function getCreateur() {return $this->createur;}
	public function getTheme1() {return $this->theme1;}
	public function getTheme2() {return $this->theme2;}
	public function getTheme3() {return $this->theme3;}
	public function getTitre() {return $this->titre;}
	public function getDateCrea() {return $this->dateCrea;}
	public function getIDalbum() {return $this->IDalbum;}
	public function getConfidentialite() {return $this->confidentialite;}
	public function getNbphotos() {return $this->nbphotos;}
	
	
	//setters
	public function setChemPhotoCouv($val) {$this->chemPhotoCouv = $val;}
	public function setCreateur($val) {$this->createur = $val;}
	public function setTheme1($val) {$this->theme1 = $val;}
	public function setTheme2($val) {$this->theme2 = $val;}
	public function setTheme3($val) {$this->theme3 = $val;}
	public function setTitre($val) {$this->titre = $val;}
	public function setDateCrea($val) {$this->dateCrea = $val;}
	public function setIDalbum($val) {$this->IDalbum = $val;}
	public function setConfidentialite($val) {$this->confidentialite = $val;}
	public function setNbphotos($val) {$this->nbphotos = $val;}

	
	public function setBdd($val) {$thid->bdd = $val;}
	
	
	//methodes
	
	//constructeur qui prend en argument la bdd et qui initialise l'album avec des valeurs par défaut
	public function __construct( $DB ) {
		$this->bdd = $DB;
		
		$this->setChemPhotoCouv('NULL');
		$this->setCreateur('NULL');
		$this->setTheme1('NULL');
		$this->setTheme2('NULL');
		$this->setTheme3('NULL');
		$this->setTitre('NULL');
		$this->setDateCrea('00/00/00');
		$this->setIDalbum('0000');
		$this->setConfidentialite('NULL');
		$this->setNbphotos('NULL');

		
	}
	
	//hydrate l'album actuelle avec les infos de l'album dont l'id est en paramètre.
	public function charge($id){
		$reponse = $this->bdd->query("SELECT * FROM album WHERE IDalbum = \"$id\"");
		
		$donnees = $reponse->fetch();
		//test, sinon, code en comentaire
		$this.remplie($donnees);
		$reponse->closeCursor(); 

	}
	
	//remplie les attributs avec les infos du tableau en arg
	public function remplie(array $infos){
		if (array_key_exists('chemPhotoCouv',$infos)){$this->setChemPhotoCouv($infos['chemPhotoCouv']);}
		if (array_key_exists('createur',$infos)){$this->setCreateur($infos['createur']);}
		if (array_key_exists('theme1',$infos)){$this->setTheme1($infos['theme1']);}
		if (array_key_exists('theme2',$infos)){$this->setTheme2($infos['theme2']);}
		if (array_key_exists('theme3',$infos)){$this->setTheme3($infos['theme3']);}
		if (array_key_exists('titre',$infos)){$this->setTitre($infos['titre']);}
		if (array_key_exists('dateCrea',$infos)){$this->setDateCrea($infos['dateCrea']);}
		if (array_key_exists('IDalbum',$infos)){$this->setIDalbum($infos['IDalbum']);}
		if (array_key_exists('confidentialite',$infos)){$this->setConfidentialite($infos['confidentialite']);}
		if (array_key_exists('nbphotos',$infos)){$this->setNbphotos($infos['nbphotos']);}

	}
	
	
	
		
	
	
	// methodes de management de photos
	//ajoute (INSERT)
	public function ajoutAbdd(){
		$sql="INSERT INTO `album`(  `chemPhotoCouv`, 
									`createur`, 
									`theme1`, 
									`theme2`, 
									`theme3`, 
									`titre`, 
									`dateCrea`, 
									`IDalbum`, 
									`confidentialite`, 
									`nbphotos`   ) 
		VALUES (
		\"$this->chemPhotoCouv\",
		\"$this->createur\",
		\"$this->theme1\",
		\"$this->theme2\",
		\"$this->theme3\",
		\"$this->titre\",
		\"$this->dateCrea\",
		\"$this->IDalbum\",  
		\"$this->confidentialite\",  
		\"$this->nbphotos\")";
		$this->bdd->exec($sql);
		//echo "$sql";
	}
	//efface (DELETE)
	public function efface(){
		global $bdd;
		$sql = "DELETE FROM `album` WHERE IDalbum = \"$this->IDalbum\"";
		$this->bdd->exec($sql);
	}
	

	
	//update (UPDATE) (on ne touche pas au pseudo)
	//methode qui charge un album dans $this mais avec un id déja existant, et le met sur la base en écrasant l'ancien
	public function update(array $infos){
		$this->remplie($infos);
		
		$sql = "UPDATE `album` SET 
		`chemPhotoCouv`=\"$this->chemPhotoCouv\",
		`createur`=\"$this->createur\",
		`theme1`=\"$this->theme1\",
		`theme2`=\"$this->theme2\",
		`theme3`=\"$this->theme3\",
		`titre`=\"$this->titre\",
		`dateCrea`=\"$this->dateCrea\" 
		`nbphotos`=\"$this->nbphotos\" 
		`confidentialite`=\"$this->confidentialite\" 
		WHERE `IDalbum` = \"this->IDalbum\"";
		
		$this->bdd->exec($sql);
		
	}
}

?>