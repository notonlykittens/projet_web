<?php 
session_start(); 
require 'utilisateur_class.php';

//conexion à la bdd
try{
	$bdd = new PDO('mysql:host=localhost;dbname=projet_web;charset=utf8', 'root', '');
	$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
}
catch(Exception $e) {
	echo $e->getMessage();
	return;	
}
$utilisateur = new Utilisateur($bdd);
$utilisateur->charge($_SESSION['pseudo']);
?>

<!DOCTYPE html>

<html>
    <head>
        <script type="text/javascript" src="site.js"></script>  
        <link rel="stylesheet" type="text/css" href="site2.css"/> 
        <title>PHOTauTOP</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    
    
    
<body>
    <div class="image" style="background-image:url(papier_bleu.gif)"><img src="papier_bleu.gif" alt="" /></div>
    <div class="image" style="background-image:url(montagne.jpg)"><img src="montagne.jpg" alt="" /></div>
    <div class="image" style="background-image:url(foret.jpg)"><img src="foret.jpg" alt="" /></div> 

    
    
    <div  id="barretache">
    <table>
        <tr>
            <td>    
            <a href="actu.php"><button type=submit><img src=appreil.jpg width="40px" height="40px" ></button></a>
            </td>
            
            <td>
                <input style="height:40px;font-size:14pt"; size="60" value="Recherchez ce qui vous ferait plaisir..."; id="recherche"; onfocus=rechercher(); onblur=search(); >   
            </td>
            <td class="ecart"></td>
            <td>
                <a href="monprofil.php" class="lien_barre">Mon Profil</a>			
            </td>
			<td class="ecart"></td>
			<td <a href="mesAlbums.php" class="lien_barre">Accéder à mes albums</a>
			<td class="ecart"></td>
			<td>
				<a class="lien_barre">Déconnexion</a>
			</td>
			
			
    </table>
    </div>
    <div id="titre">SITE</div>    
		
		<table width=100% border="1px";>
		<td>
		<a href="ajout_album.php" <button type=button style="width:100%">CREER UN NOUVEL ALBUM</button></a>
		</td>
		</table>
		
		<table class="album" border="1px"; >
<?php
$sql="SELECT * FROM album WHERE `createur` = \"$utilisateur->pseudo\" ";
$reponse = $bdd->query($sql);

// On affiche chaque album un à un
while ($donnees = $reponse->fetch())
{
?>
    <tr>
		<td>
			<form id="fauxAjax" method="post" action="">
			<input type="hidden" name="albumClic" value="$donnees['IDalbum']"/>
			</form>
			<a href='photos.php' onclick='document.getElementById("fauxAjax").submit()'><img src=<?php echo $donnees['chemPhotoCouv'];?>></a>
		</td>
						
		<td>
			<ul>
				<li>Nom : <?php echo $donnees['titre'];?></li>
				<li>Date de création : <?php echo $donnees['dateCrea'];?></li>
				<li>theme1 : <?php echo $donnees['theme1'];?></li>
				<li>theme2 : <?php echo $donnees['theme2'];?></li>
				<li>theme3 : <?php echo $donnees['theme3'];?></li>
			</ul>
		</td>
	</tr>
<?php
}

$reponse->closeCursor(); // Termine le traitement de la requête

//récupération de l'album clicé dans une variable de session
if (isset($_POST['albumClic'])){
	$_SESSION['album']=$_POST['albumClic'];
	echo "yolo";
}
?>
		</table>
	        </body>
    
</html>

