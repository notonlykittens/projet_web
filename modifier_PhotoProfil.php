<?php 
session_start();
require 'utilisateur_class.php';
require 'photo_class.php';

//conexion à la bdd
try{
	$bdd = new PDO('mysql:host=localhost;dbname=projet_web;charset=utf8', 'root', '');
	$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
}
catch(Exception $e) {
	echo $e->getMessage();
	return;	
}

$utilisateur = new Utilisateur($bdd);
$utilisateur->charge($_SESSION['pseudo']);
?>

<html>
	<head>
		<title>ajout_photo</title>
	</head>
	<body>
		<form method="post" action="" enctype="multipart/form-data">
			theme1:<input type="text" name="theme1" /><br />
			theme2:<input type="text" name="theme2" /><br />
			theme3:<input type="text" name="theme3" /><br />
			datePrise:<input type="date" name="datePrise" /><br />
			lieu:<input type="text" name="lieu" /><br />
			(confidentialie:)<input type="text" name="confidentialite" /><br />   <!-- à mettre en truc à cocher-->
			choisir un fichier:<input type="file" name="photoFile" /><br />
			<input type="submit" value="envoyer" name="submitbutton" />
		</form>	
		<a href="monprofil.php" ><button type=button >retour au profil</button></a>
	</body>
</html>

<?php
if (isset($_POST['submitbutton'])){
	if ( !empty($_POST['theme1'])
			AND !empty($_POST['theme2'])
			AND !empty($_POST['theme3'])
			AND !empty($_POST['datePrise'])
			AND !empty($_POST['lieu'])
			AND !empty($_POST['confidentialite'])
			AND $_FILES['photoFile']['error'] == 0){
		
		//on vérifie qu'on a bien une image
		$extensions_valides = array( 'jpg' , 'jpeg' , 'gif' , 'png' );
		$extension_upload = strtolower(  substr(  strrchr($_FILES['photoFile']['name'], '.')  ,1)  );
		if ( in_array($extension_upload,$extensions_valides) ){
			//on déplace le fichier uploadé dans le dossier photo du serveur
			$id = md5(uniqid(rand(), true));
			$chemin = "photos/{$id}.{$extension_upload}";
			$resultat = move_uploaded_file($_FILES['photoFile']['tmp_name'],$chemin);
		}


		//création d'un objet maphoto avec les spécificités voulues du form
		$infos = array ('auteur' => $utilisateur->pseudo,
						'theme1' => $_POST["theme1"],
						'theme2' => $_POST["theme2"],
						'theme3' => $_POST["theme3"],
						'datePrise' => $_POST["datePrise"],
						'lieu' => $_POST["lieu"],
						'IDphoto' => $id,
						'confidentialite' => $_POST["confidentialite"],
						'chemin' => $chemin);
						
		$maphoto = new Photo($bdd);
		$maphoto->remplie($infos);
		$maphoto->ajoutAbdd();
		$infos = array ('chemPhotoProfil' => $chemin);
		$utilisateur->update($infos);
		
	}
	else{
		echo "veuillez remplir tous les champs.";
	}
}
else{
	//echo "pas isset submitbutton";
}

?>