<?php 
session_start(); 
require 'utilisateur_class.php';

//conexion à la bdd
try{
	$bdd = new PDO('mysql:host=localhost;dbname=projet_web;charset=utf8', 'root', '');
	$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
}
catch(Exception $e) {
	echo $e->getMessage();
	return;	
}

$utilisateur = new Utilisateur($bdd);
$utilisateur->charge($_SESSION['pseudo']);
?>

<html>
	<head>
		<title>modifier_profil</title>
	</head>
	<body>
		<form method="post" action="" enctype="multipart/form-data">
			centreInteret1:<input type="text" name="centreInteret1" /><br />
			centreInteret2:<input type="text" name="centreInteret2" /><br />
			centreInteret3:<input type="text" name="centreInteret3" /><br />
			description:<input type="text" name="description" /><br />
			<input type="submit" value="envoyer" name="submitbutton" />
		</form>	
		<a href="monprofil.php" ><button type=button >retour au profil</button></a>
	</body>
</html>

<?php
if (isset($_POST['submitbutton'])){
	if (!empty($_POST['centreInteret1'])
			AND !empty($_POST['centreInteret2'])
			AND !empty($_POST['centreInteret3'])
			AND !empty($_POST['description'])){
		
		//création d'un objet infos avec les spécificités voulues du form
		$infos = array ('pseudo' => $utilisateur->pseudo,
						'statu' => $utilisateur->statu,
						'centreInteret1' => $_POST["centreInteret1"],
						'centreInteret2' => $_POST["centreInteret2"],
						'centreInteret3' => $_POST["centreInteret3"],
						'dateInscription' => $utilisateur->dateInscription,
						'mail' => $utilisateur->mail,
						'motDePasse' => $utilisateur->motDePasse,
						'nom' => $utilisateur->nom,
						'prenom' => $utilisateur->prenom,
						'dateNaissance' => $utilisateur->dateNaissance,
						'description' => $_POST["description"],);
						
		$monutilisateur = new Utilisateur($bdd);
		$monutilisateur->update($infos);
		//echo "\nphoto uploadee !";		
	}
	else{
		//echo "pas !empty les bails";
	}
}
else{
	//echo "pas isset submitbutton";
}

?>
