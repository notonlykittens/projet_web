<?php
class Photo {
	// attibuts
	private $auteur;
	private $theme1;
	private $theme2;
	private $theme3;
	private $datePrise;
	private $lieu;
	private $album;
	private $IDphoto;
	private $confidentialite;
	private $chemin;
	
	private $bdd;
	
	//geters
	public function getAuteur() {return $this->auteur;}
	public function getTheme1() {return $this->theme1;}
	public function getTheme2() {return $this->theme2;}
	public function getTheme3() {return $this->theme3;}
	public function getDatePrise() {return $this->datePrise;}
	public function getLieu() {return $this->lieu;}
	public function getAlbum() {return $this->album;}
	public function getIDphoto() {return $this->IDphoto;}
	public function getConfidentialite() {return $this->confidentialite;}
	public function getChemin() {return $this->chemin;}
	
	//setters
	public function setAuteur($val) {$this->auteur = $val;}
	public function setTheme1($val) {$this->theme1 = $val;}
	public function setTheme2($val) {$this->theme2 = $val;}
	public function setTheme3($val) {$this->theme3 = $val;}
	public function setDatePrise($val) {$this->datePrise = $val;}
	public function setLieu($val) {$this->lieu = $val;}
	public function setAlbum($val) {$this->album = $val;}
	public function setIDphoto($val) {$this->IDphoto = $val;}
	public function setConfidentialite($val) {$this->confidentialite = $val;}
	public function setChemin($val) {$this->chemin = $val;}
	
	public function setBdd($val) {$thid->bdd = $val;}
	
	//methodes
	
	//constructeur qui prend en argument la bdd et qui initialise la photo avec des valeurs par défaut
	public function __construct( $DB ) {
		$this->bdd = $DB;
		
		$this->setAuteur('NULL');
		$this->setTheme1('NULL');
		$this->setTheme2('NULL');
		$this->setTheme3('NULL');
		$this->setDatePrise('00/00/00');
		$this->setLieu('NULL');
		$this->setAlbum('NULL');
		$this->setIDphoto('0000');
		$this->setConfidentialite('NULL');
		$this->setChemin('NULL');
		
	}
	
	//hydrate la photo actuelle avec les infos de la photo dont l'id est en paramètre.
	public function charge($id){
		$reponse = $this->bdd->query("SELECT * FROM photo WHERE IDphoto = \"$id\"");
		
		$donnees = $reponse->fetch();
		
		$this->setAuteur($donnees['auteur']);
		$this->setTheme1($donnees['theme1']);
		$this->setTheme2($donnees['theme2']);
		$this->setTheme3($donnees['theme3']);
		$this->setDatePrise($donnees['datePrise']);
		$this->setLieu($donnees['lieu']);
		$this->setAlbum($donnees['album']);
		$this->setIDphoto($donnees['IDphoto']);
		$this->setConfidentialite($donnees['confidentialite']);
		$this->setChemin($donnees['chemin']); 
			
		$reponse->closeCursor(); 

	}
	
	//remplie les attributs avec les infos du tableau en arg
	public function remplie(array $infos){
		if (array_key_exists('auteur',$infos)){$this->setAuteur($infos['auteur']);}
		if (array_key_exists('theme1',$infos)){$this->setTheme1($infos['theme1']);       }
		if (array_key_exists('theme2',$infos)){$this->setTheme2($infos['theme2']);     }
		if (array_key_exists('theme3',$infos)){$this->setTheme3($infos['theme3']);     }
		if (array_key_exists('datePrise',$infos)){$this->setDatePrise($infos['datePrise']); }
		if (array_key_exists('lieu',$infos)){$this->setLieu($infos['lieu']);}
		if (array_key_exists('album',$infos)){$this->setAlbum($infos['album']);  }
		if (array_key_exists('IDphoto',$infos)){$this->setIDphoto($infos['IDphoto']);  }
		if (array_key_exists('confidentialite',$infos)){$this->setConfidentialite($infos['confidentialite']);}
		if (array_key_exists('chemin',$infos)){$this->setChemin($infos['chemin']);}
	}
	
	
	
	
		
	
	
	// methodes de management de photos
	//ajoute (INSERT)
	public function ajoutAbdd(){
		$sql="INSERT INTO `photo`(`auteur`, `theme1`, `theme2`, `theme3`, `datePrise`, `lieu`, `album`, `IDphoto`, `confidentialite`, `chemin`) 
		VALUES (
		\"$this->auteur\",
		\"$this->theme1\",
		\"$this->theme2\",
		\"$this->theme3\",
		\"$this->datePrise\",
		\"$this->lieu\",
		\"$this->album\",
		\"$this->IDphoto\",
		\"$this->confidentialite\",
		\"$this->chemin\")";  
		$this->bdd->exec($sql);
		//echo "$sql";
	}
	//efface (DELETE)
	public function efface(){
		global $bdd;
		$sql = "DELETE FROM `photo` WHERE IDphoto = \"$this->IDphoto\"";
		$this->bdd->exec($sql);
	}
	
	
	
	//chemin, retourne le chemin de la photo
	public function chemin($id){
		$reponse = $this->bdd->query('SELECT * FROM photo WHERE IDphoto=\"$id\"');
		return $reponse->fetch()['chemin'];
	}
	
	//update (UPDATE) (on ne touche pas à l'IDphoto)
	//mmethode qui charge une photo dans $this mais avec un identifiant déja existant et la met sur la base en écrasant l'ancienne
	public function update(array $infos){
		$this->remplie($infos);
		
		$sql = "UPDATE `photo` SET 
		`auteur`=\"$this->auteur\",
		`theme1`=\"$this->theme1\",
		`theme2`=\"$this->theme2\",
		`theme3`=\"$this->theme3\",
		`datePrise`=\"$this->datePrise\",
		`lieu`=\"$this->lieu\",
		`album`=\"$this->album\",
		`confidentialite`=\"$this->confidentialite\",
		`chemin`=\"$this->chemin\" WHERE `IDphoto` = \"this->IDphoto\"";
		
		$this->bdd->exec($sql);
		
	}
}

?>