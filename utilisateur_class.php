<?php

class Utilisateur {
	// attibuts
	public $pseudo;
	public $statu;
	public $centreInteret1;
	public $centreInteret2;
	public $centreInteret3;
	public $dateInscription;
	public $mail;
	public $motDePasse;
	public $nom;
	public $prenom;
	public $dateNaissance;
	public $description;
	public $chemPhotoProfil;
	
	private $bdd;
	
	//geters
	public function getPseudo() {return $this->pseudo;}
	public function getStatu() {return $this->statu;}
	public function getCentreInteret1() {return $this->centreInteret1;}
	public function getCentreInteret2() {return $this->centreInteret2;}
	public function getCentreInteret3() {return $this->centreInteret3;}
	public function getDateInscription() {return $this->dateInscription;}
	public function getMail() {return $this->mail;}
	public function getMotDePasse() {return $this->motDePasse;}
	public function getNom() {return $this->nom;}
	public function getPrenom() {return $this->prenom;}
	public function getDateNaissance() {return $this->dateNaissance;}
	public function getDescription() {return $this->description;}
	public function getChemPhotoProfil() {return $this->chemPhotoProfil;}
	
	//setters
	public function setPseudo($val) {$this->pseudo = $val;}
	public function setStatu($val) {$this->statu = $val;}
	public function setCentreInteret1($val) {$this->centreInteret1 = $val;}
	public function setCentreInteret2($val) {$this->centreInteret2 = $val;}
	public function setCentreInteret3($val) {$this->centreInteret3 = $val;}
	public function setDateInscription($val) {$this->dateInscription = $val;}
	public function setMail($val) {$this->mail = $val;}
	public function setMotDePasse($val) {$this->motDePasse = $val;}
	public function setNom($val) {$this->nom = $val;}
	public function setPrenom($val) {$this->prenom = $val;}
	public function setDateNaissance($val) {$this->dateNaissance= $val;}
	public function setDescription($val) {$this->description = $val;}
	public function setChemPhotoProfil($val) {$this->chemPhotoProfil = $val;}
	
	public function setBdd($val) {$thid->bdd = $val;}
	
	
	//methodes
	
	//constructeur qui prend en argument la bdd et qui initialise l'utilisateur avec des valeurs par défaut
	public function __construct( $DB ) {
		$this->bdd = $DB;
		
		$this->setPseudo('NULL');
		$this->setStatu('NULL');
		$this->setCentreInteret1('NULL');
		$this->setCentreInteret2('NULL');
		$this->setCentreInteret3('NULL');
		$this->setDateInscription('00/00/00');
		$this->setMail('NULL');
		$this->setMotDePasse('NULL');
		$this->setNom('NULL');
		$this->setPrenom('NULL');
		$this->setDateNaissance('00/00/00');
		$this->setDescription('NULL');
		$this->setChemPhotoProfil('NULL');
		
	}
	
	//hydrate la photo actuelle avec les infos de l'utilisateur dont le pseudo est en paramètre.
	public function charge($pseudo){
		$reponse = $this->bdd->query("SELECT * FROM utilisateur WHERE pseudo = \"$pseudo\"");
		
		$donnees = $reponse->fetch();
		//test, sinon, code en comentaire
		$this->remplie($donnees);
	/*	
		$this->setPseudo($donnees['pseudo']);
		$this->setStatu($donnees['statu']);
		$this->setCentreInteret1($donnees['centreInteret1']);
		$this->setCentreInteret2($donnees['centreInteret2']);
		$this->setCentreInteret3($donnees['centreInteret3']);
		$this->setDateInscription($donnees['dateInscription']);
		$this->setMail($donnees['mail']);
		$this->setMotDePasse($donnees['motDePasse']);
		$this->setNom($donnees['nom']);
		$this->setPrenom($donnees['prenom']);
		$this->setDateNaissance($donnees['dateNaissance']);
		$this->setDescription($donnees['description']);
		$this->setChemPhotoProfil($donnees['chemPhotoProfil']);
	*/	
		$reponse->closeCursor(); 

	}
	
	//remplie les attributs avec les infos du tableau en arg
	public function remplie(array $infos){
		if (array_key_exists('pseudo',$infos)){$this->setPseudo($infos['pseudo']);}
		if (array_key_exists('statu',$infos)){$this->setStatu($infos['statu']);}
		if (array_key_exists('centreInteret1',$infos)){$this->setCentreInteret1($infos['centreInteret1']);}
		if (array_key_exists('centreInteret2',$infos)){$this->setCentreInteret2($infos['centreInteret2']);}
		if (array_key_exists('centreInteret3',$infos)){$this->setCentreInteret3($infos['centreInteret3']);}
		if (array_key_exists('dateInscription',$infos)){$this->setDateInscription($infos['dateInscription']);}
		if (array_key_exists('mail',$infos)){$this->setMail($infos['mail']);}
		if (array_key_exists('motDePasse',$infos)){$this->setMotDePasse($infos['motDePasse']);}
		if (array_key_exists('nom',$infos)){$this->setNom($infos['nom']);}
		if (array_key_exists('prenom',$infos)){$this->setPrenom($infos['prenom']);}
		if (array_key_exists('dateNaissance',$infos)){$this->setDateNaissance($infos['dateNaissance']);}
		if (array_key_exists('description',$infos)){$this->setDescription($infos['description']);}
		if (array_key_exists('chemPhotoProfil',$infos)){$this->setChemPhotoProfil($infos['chemPhotoProfil']);}
	}
	
	
	
		
	
	
	// methodes de management de photos
	//ajoute (INSERT)
	public function ajoutAbdd(){
		$sql="INSERT INTO `utilisateur`(  `pseudo`, 
									`statu`, 
									`centreInteret1`, 
									`centreInteret2`, 
									`centreInteret3`, 
									`dateInscription`, 
									`mail`, 
									`motDePasse`, 
									`nom`, 
									`prenom`, 
									`dateNaissance`, 
									`description`, 
									`chemPhotoProfil`   ) 
		VALUES (
		\"$this->pseudo\",
		\"$this->statu\",
		\"$this->centreInteret1\",
		\"$this->centreInteret2\",
		\"$this->centreInteret3\",
		\"$this->dateInscription\",
		\"$this->mail\",
		\"$this->motDePasse\",
		\"$this->nom\",
		\"$this->prenom\", 
		\"$this->dateNaissance\",
		\"$this->description\",
		\"$this->chemPhotoProfil\")";
		//echo "$sql";
		$this->bdd->exec($sql);
	}
	//efface (DELETE)
	public function efface(){
		global $bdd;
		$sql = "DELETE FROM `utilisateur` WHERE pseudo = \"$this->pseudo\"";
		$this->bdd->exec($sql);
	}
	

	
	//update (UPDATE) (on ne touche pas au pseudo)
	//methode qui charge un utilisateur dans $this mais avec un pseudo déja existant et le met sur la base en écrasant l'ancien
	public function update(array $infos){
		$this->remplie($infos);
		
		$sql = "UPDATE `utilisateur` SET 
		`statu`=\"$this->statu\",
		`centreInteret1`=\"$this->centreInteret1\",
		`centreInteret2`=\"$this->centreInteret2\",
		`centreInteret3`=\"$this->centreInteret3\",
		`dateInscription`=\"$this->dateInscription\",
		`mail`=\"$this->mail\",
		`motDePasse`=\"$this->motDePasse\",
		`nom`=\"$this->nom\",
		`prenom`=\"$this->prenom\", 
		`dateNaissance`=\"$this->dateNaissance\",
		`description`=\"$this->description\" ,
		`chemPhotoProfil`=\"$this->chemPhotoProfil\"
		WHERE `pseudo` = \"$this->pseudo\"";
		//echo "$sql";
		$this->bdd->exec($sql);
		
	}
}

?>